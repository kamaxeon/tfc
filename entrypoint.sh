#! /bin/bash

python3 manage.py migrate

if [[ "${IS_PRODUCTION:-}" = [Tt]rue ]] ; then
    supervisord # Not tested
else
    python3 manage.py wait_for_db
    supervisord -c /code/configs/supervisor-app-devel.ini
fi

