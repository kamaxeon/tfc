FROM python:3.10-slim

RUN mkdir -p /code
COPY . /code

WORKDIR /code
RUN pip install --no-cache-dir -e .

# Configure supervisord
RUN mkdir -p /run/supervisor /etc/supervisord.d /var/log/supervisor \
    && chmod g+rwx /var/log/supervisor /run/supervisor 
COPY configs/supervisor-app.ini /etc/supervisord.d/

EXPOSE 8000
CMD ["/code/entrypoint.sh"]

